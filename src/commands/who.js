module.exports = async (msg) => {
  if (msg.trim() !== '*who' && msg.indexOf('JasperBot') === -1) {
    return false;
  }
  return `Hey, I'm JasperBot (${process.env.CI_COMMIT_SHORT_SHA || 'xxx'}) and Jasper Noorkõiv is my creator!
You can find my code at: https://gitlab.com/JasperNoorkoiv/DiscordBot`;
};
